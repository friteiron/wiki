# Credits
This file contains all the people who've ever contributed to MRCL.

### Note
**This credit file is often not up to date. Make a merge request or an issue if the information is not accurate.**


## Builds
#### Old Kadic:  [Planet Minecraft](https//www.planetminecraft.com/project/codelyokokadicacademy/)

#### Refurbished old Kadic
* Alexmario5 
* Ten 

#### New Kadic (base) 
* KaruzoHikari 
* GoodOldJack12

#### New Kadic (decoration) 
* KaruzoHikari 
* TankadiN 
* GoodOldJack12
* Luclyoko 

#### New factory
* TankadiN 
* TheLeech 
* KaruzoHikari 

#### Carthage 
* Noojin 
* Ten 
#### Ice sector 
* Noojin 
* Ten 
#### Forest replica and derivatives 
* Noojin 
* Ten 
## Gameplay

#### Scanner transition 
* Jack (code)
* Noojin (colors)
* Alexmario5 (redstone)
* Ten (redstone)
* KaruzoHikari (code)
#### Scanners
* KaruzoHikari (Animations, models, code)
* GoodOldJack12 (code)

#### Old Superscan system (and general SC redstone) 
* Ten 
#### Old Scanner system 
* Ten 

#### Factory door
* KaruzoHikari 
* Alexmario5 
* Ten

#### Advancements
* Alexmario5 
* KaruzoHikari 

#### Project Carthage Plugin (rttp, devirt, superscan frontend, tower activation front end)
* GoodOldJack12
* KaruzoHikari 

#### Frontier
* Alexmario5 

#### Old devirt system
* Alexmario5 

#### Transporter
* Ten 
* Alexmario5 
* KaruzoHikari 

## Assets

#### Texture pack 
* Ten 
* Alexmario5 
* Noojin 
* Franz hopper 

#### Misc sounds
* Juleic1123 

#### Server logo(s)
* TankadiN 

#### Translations
 * French 
   - Alexmario5 
 * Spanish 
   - KaruzoHikari 
 * Italian 
   - 3DGamer 
 * Romanian
   - Survinar
 * Translation code
   - KaruzoHikari

A huge thanks to all the Alpha Testers and to Sky for financially backing the project,
and to our supporters on Patreon and Discord as well ^^ 

Want your name here? apply for staff today!
