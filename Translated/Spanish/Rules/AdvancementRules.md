[English](/Rules/AdvancementRules.md) | [Français](/Translated/French/Rules/AdvancementRules.md) | [Romanian](/Translated/Romanian/Rules/AdvancementRules.md)

---
# Reglas sobre Logros
Aquí puedes encontrar algunas explicaciones extra sobre la regla de que no puedes decirle a alguien cómo o dónde conseguir un logro.

En general, no puedes decirle a nadie como completar un logro, como "Ah sí, el Diario de Nathan está en (estas coordenadas)" o "Simplemente busca en (este edificio)"

**Tampoco está permitido preguntar sobre los logros.** (pensamos que eso era obvio).

## Trabajar juntos
Si tú y tus amigos queréis conseguir un logro juntos, podéis hacerlo **SI** ninguno de vosotros ha conseguido el logro antes (no puedes llevar a alguien a un logro).

Además, no podéis dividir el trabajo. Tenéis que buscarlo juntos como una unidad. No puedes decirle a tus amigos que vayan a buscar en el sitio A mientras tú vas al sitio B. En resumen: **tenéis que permanecer juntos**, y si alguno de tus amigos encuentra algo accidentalmente cuando está solo, no puede deciros dónde está.

## Excepciones

### Mecánicas de Juego
**Regla general: puedes explicar mecánicas de juego a otros jugadores.**

Esto significa que puedes explicar a otras personas como hacer las distintas acciones en el juego. Estas acciones pueden llevar a un logro, así que están exentas de la regla.

Encuentra la lista [aquí](#List)
### Logros Cooperativos
También hay una excepción para los logros que necesitan más de una persona para ser completados.

Puedes, por supuesto, pedirle a otras personas que se unan a ti para estos logros. De todas formas, te recomendamos que no spoilees los detalles exactos de los logros, para hacerlo más interesante para los demás.

### Lista
Aquí está la lista de logros que tienes permitido explicar, por estar relacionados con mecánicas de juego o ser cooperativos.

**Esta lista puede estar desactualizada, así que pregunta a un Admin cuando tengas dudas.**
- Kadic
    * ¿Vas a terminarte eso?
    * Salsa... Necesita más salsa
    * Cordero y patatas de poste
    * El Superbuen Chico
    * Amante de Windows
    * D.E.P.
    * ¿Pero por qué es gris?
- Fábrica
    * Déjà Vu
- Lyoko
    * Todos los logros (en la pestaña de Lyoko)

**Recuerda que ningún logro del Sector 5 está exentos**


## Penalizaciones
Si te encuentran ayudando a otros con logros, una de las siguientes cosas puede pasarte. Puedes ser:
* Advertido
* Excluido de la obtención del rango de Veterano
* Baneado temporalmente
* Baneado permanentemente

Normalmente estos castigos se aplicarán en ese orden, siempre y cuando no rompas las normas intencionalmente.