[English](/Rules/NamingRules.md) | [Français](/Translated/French/Rules/NamingRules.md) | [Romanian](/Translated/Romanian/Rules/NamingRules.md)

---
# Reglas sobre nicks
Tenemos reglas muy específicas para nombres de usuario en Discord y Minecraft.

## Nombres que no están permitidos
No puedes tener un nombre de usuario que:
* Sea parecido al nombre de un personaje de la serie
* Contenga carácteres no identificables
* No pueda ser referenciado como nick
* No pueda ser mencionado fácilmente en Discord

## Qué hacer si tienes un nombre baneado
Depende. Algunos escenarios son:

### El nombre baneado es tu nombre de usuario de Minecraft
Si posees una cuenta de Minecraft legítimamente, y no quieres cambiar tu nombre de usuario en mojang.com, pídele a un mod que cambie tu nombre de usuario a otra cosa.

**Si no posees Minecraft, cambia tu nick en el launcher o *serás* baneado**

### El nombre baneado es tu nombre de usuario de Discord
Si no quieres cambiar tu nombre de usuario de Discord, usa la función de "Renombrar en Servidor" (o lo haremos por ti, y no te gustará lo que elijamos)

## Ejemplos de nombre baneados
### 11ε ƉơƈŧēůƦ
No está permitido porque no contiene carácteres normales
### Aelita_Shaeffer
No está permitido porque es el nombre de un personaje de CL
### Jeremie
Misma razón que arriba. Si tu nombre realmente es Jeremie, contacta con los mods.
### gksqzeoKKSdqslAZRf
No está permitido porque no se puede referenciar como nombre. No puedes esperar que alguien diga "Hey, gksqzeoKKSdqslAZRf, ¿qué tal?"