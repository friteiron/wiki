# MRCL WIKI
This repository contains information on [MRCL](http://mrcl.cc) like rules, credits and other info.

# Table Of Contents
## [Rules](Rules/Rules.md)
### [Channel Usage](Rules/ChannelUsage.md)
### [Username Rules](Rules/NamingRules.md)
# Contributing
You're free to make an issue when you see a typo, or when you want a specific piece of information added to the repository. You can also make merge requests if you wish to directly add content.